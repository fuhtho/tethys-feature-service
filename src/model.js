const config = require('../config/default.json');
const fetch = require('node-fetch');
const convert = require('xml-js');
const proj4 = require('proj4');

const wgs84 =
  'GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137.0,298.257223563]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433],AUTHORITY["EPSG",4326]]';
const webMercator =
  'PROJCS["WGS_1984_Web_Mercator_Auxiliary_Sphere",GEOGCS["GCS_WGS_1984",DATUM["D_WGS_1984",SPHEROID["WGS_1984",6378137.0,298.257223563]],PRIMEM["Greenwich",0.0],UNIT["Degree",0.0174532925199433]],PROJECTION["Mercator_Auxiliary_Sphere"],PARAMETER["False_Easting",0.0],PARAMETER["False_Northing",0.0],PARAMETER["Central_Meridian",0.0],PARAMETER["Standard_Parallel_1",0.0],PARAMETER["Auxiliary_Sphere_Type",0.0],UNIT["Meter",1.0],AUTHORITY["EPSG",3857]]';

function Model(koop) {}

// A Model is a javascript function that encapsulates my custom data access code.
// Each model should have a getData() function to fetch the geo data
// and format it into a geojson
Model.prototype.getData = async function (req, callback) {
  let json = await fetchJSON(config.tethys.url);
  let token, features;
  if (json['OAI-PMH']['ListRecords']) {
    token = getToken(json);
    features = getFeatures(json);
  }

  while (token !== undefined) {
    json = await fetchJSON(config.tethys.resumptionUrl + token);
    if (json['OAI-PMH']['ListRecords']) {
      token = getToken(json);
      features = features?.concat(getFeatures(json));
    }
  }

  // ttl in seconds for caching
  let ttl = 604800;
  const geojson = {
    type: 'FeatureCollection',
    metadata: {
      name: 'datasets',
    },
    ttl: ttl,
    crs: {
      type: 'WGS 1984 Web Mercator',
      properties: {
        name: 'urn:ogc:def:crs:EPSG::3857',
      },
    },
    features: features,
  };

  // the callback function expects a geojson for its second parameter
  callback(null, geojson);
};

const fetchJSON = async (url) => {
  const response = await fetch(url);
  const text = await response.text();
  const xml = convert.xml2js(text, { compact: true, spaces: 4 });
  return xml;
};

const getToken = (json) => {
  const token = json['OAI-PMH']['ListRecords']['resumptionToken'];
  if (token) return token['_text'];
};

const getFeatures = (json) => {
  const records = json['OAI-PMH']['ListRecords']['record'];

  return records?.map((record) => {
    const coverage = record['metadata']['resource']['geoLocations']['geoLocation']['geoLocationBox'];
    const x1 = coverage['westBoundLongitude']['_text'];
    const x2 = coverage['eastBoundLongitude']['_text'];
    const y1 = coverage['southBoundLatitude']['_text'];
    const y2 = coverage['northBoundLatitude']['_text'];

    let titleEN, titleDE;
    for (let element of record['metadata']['resource']['titles']['title']) {
      if (element['_attributes']['xml:lang'] === 'en') {
        titleEN = element['_text'];
      } else if (element['_attributes']['xml:lang'] === 'de' && element['_attributes']['titleType'] !== 'Subtitle') {
        titleDE = element['_text'];
      }
    }

    const dataType = record['header']['setSpec'][0]['_text'];
    const date = record['header']['datestamp']['_text'];
    const doi = record['header']['identifier']['_text'].match(/(?<=oai:tethys.at:)\d+/g)[0];

    const coordinatesWGS84 = [
      [x1, y1],
      [x2, y1],
      [x2, y2],
      [x1, y2],
      [x1, y1],
    ];

    const coordinatesWebMercator = coordinatesWGS84.map((point) =>
      proj4(wgs84, webMercator, [parseFloat(point[0]), parseFloat(point[1])])
    );

    return {
      type: 'Feature',
      properties: {
        title_de: titleDE,
        title_en: titleEN,
        data_type: dataType,
        date: new Date(date).toLocaleString(),
        url: 'https://doi.tethys.at/10.24341/tethys.' + doi,
      },
      geometry: {
        type: 'Polygon',
        coordinates: [coordinatesWebMercator],
      },
    };
  });
};

Model.prototype.createKey = function (req) {
  let key = req.url.split('/')[1];
  if (req.params.host) key = [key, req.params.host].join('::');
  if (req.params.id) key = [key, req.params.id].join('::');
  // if (req.params.layer) key = [key, req.params.layer].join('::')
  return key;
};

module.exports = Model;
